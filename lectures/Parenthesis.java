import java.util.Stack;

class Parenthesis {

    public static void main(String[] args) {
      String toCheck = "(())";
      boolean success = check(toCheck);
      System.out.println(success);
    }

    public static boolean check(String input) {
      Stack<Character> checker = new Stack<Character>();

      for (int i = 0; i < input.length(); i++) {
        char c = input.charAt(i);

        if(c == '(' || c == '[' || c == '{') {
          checker.push(c);

        } else if (c == ')') {
            if (checker.empty()) {
                return false;
            }
            char top = checker.pop();
            if (top != '(') {
                return false;
            }

        } else if (c == ']') {
            if (checker.empty()) {
              return false;
            }
            char top = checker.pop();
            if (top != '[') {
              return false;
            }
        } else if (c == '}') {
          if (checker.empty()) {
              return false;
          }
          char top = checker.pop();
          if (top != '{') {
              return false;
            }
        }
    }

    if (checker.empty()) {
      return true;
    } else {
      return false;
    }
}
}
