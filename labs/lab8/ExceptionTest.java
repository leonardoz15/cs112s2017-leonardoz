import java.io.*;
//method for testing eception in part 1
public class ExceptionTest {

  public static void main(String[] args) throws MyException {
      String p = "Hello";
      try {
      throw new MyException(p);
    } catch (MyException e) {
      System.out.println("You've been caught");
    }
  }
}
