public class MyException extends RuntimeException {

  public MyException(String description) {
    super(description);
  }
}
