/**
 * A generic Queue interface that supports adding, peeking, and removing
 * @param <E> the type of object in the Queue
 */
public interface ExceptionQueue<E> {

	/**
	 * Adds e to the back of queue
	 * @param e the object to add
	 */
	public void add(E e);

	/**
	 * Peek at the object at the front/top of the queue, throwing an exception if that is not possible
	 * @return the object peeked at
	 */
	public E peek();

	/**
	 * Removes the object at the front/top of the queue, throwing an exception if that is not possible
	 * @return the object removed
	 */
	public E remove();

	/**
	 * Get the size of the queue
	 * @return the size
	 */
	public int size();

	/**
	 * Get the emptiness of the queue
	 * @return true if the queue is empty, false if it is not
	 */
	public boolean empty();
}
