import java.util.*;

public class ArrayQueue<E> /*implements ExceptionQueue*/ {

  private E[] queue;
  private int capacity;
  private int rear = 0;

  public ArrayQueue(int initialCapacity) {
    rear = 0;
    capacity = initialCapacity;
    queue = (E[])(new Object[capacity]);
  }
  public void Add(E t) {
    if (size() == queue.length) {
      resize();
    } //If the array is full, resize
    queue[rear] = t;
    rear++;
  }

  public E peek() throws EmptyQueueException {
    if(empty()) {
      throw new EmptyQueueException ("The queue is empty");
    }
    E first = queue[0];
    return first;
  }

  public E remove() {
    if(empty()) {
      throw new EmptyQueueException ("The queue is already empty");
    }
    E first = queue[0];
    rear--;
    for(int p = 0; p < rear; p++) {
      queue[p] = queue[p+1];
    }
    queue[rear] = null;
    return first;
  }

  public int size() {
    return rear;
  }

  public boolean empty() {
    return rear == 0;
  }
  public E getRear() {
    if(empty()) {
      return null;
    }
    else {
      return queue[rear];
    }
  }

  public void resize() {
    E[] large = (E[])(new Object[queue.length* 2]);

    for (int i = 0; i < queue.length; i++) {
      large[i] = queue[i];
    }
    large = queue;
  }

  public static void main(String[] args) {
    ArrayQueue q = new ArrayQueue(6);

    //q.remove(); throws the empty queue exception fine
    q.Add(1);
    q.Add(2);
    q.Add(3);
    q.Add(4);
    q.Add(5);

    q.remove();
    q.Add(6);

  }
}
