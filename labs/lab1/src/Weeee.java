public class Weeee {

    public static void weeee() {
		System.out.println("Weeee!");//The class just prints out "Weeee!"
		weeee();//method calling on the main class creating an infinite loop
    }

    public static void main(String[] args) {
		weeee();
    } //main class calling on the infinite loop

}
