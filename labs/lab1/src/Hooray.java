public class Hooray {

    public static void hooray() {

		while(true) {
			System.out.println("Hooray!");
    	} //Produces an infinite loop of print statements

    }

    public static void main(String[] args) {
		hooray();
    } //The main class that calls on the infinite loop

} //Hooray (class)
