//Zach Leonardo
//Lab 6
class DoublyLinkedList {

	private static class Node {

		protected int data;
		protected Node next, previous;

		public Node() {
			next = null;
			data = 0;
		} //Node (constructor)

		public Node(int d, Node n, Node p) {
			next = n;
			data = d;
			previous = p;
		} //Node (constructor)

		public void setNext(Node n) {
			next = n;
		} //to set the next link

		public void setPrev(Node p) {
			previous = p;
		} //to set the previous link

		public Node getNext() {
			return next;
		} //to get the next link

		public Node getPrev() {
			return previous;
		} //to get the previous link

	} //Node (class)


    // DoublyLinkedList stuff starts here
	private int size;
	private Node head, tail;

	public DoublyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //DoublyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		return (size == 0);
	} //isEmpty

	public void add(int newInt) {
		Node newNode = new Node(newInt, head, tail);
		if (head == null) { //empty list
			head = newNode;
			tail = head;
		}
		else {
			newNode.setNext(tail);
			tail.setPrev(newNode);
			tail = newNode; //adds to end of list
		}
		size++;
	} //add

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public int getFromEnd(int index) {
		Node currentPosition = tail;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //getFromEnd

	public void remove(int index) {
		if(index == 0) {
			//removing single node:
			if(size == 1) {
				head = null;
				tail = null;
				size--;
			}
			//removing head:
			head = head.getNext();
			head.getPrev().setNext(null);
			head.setPrev(null);
			size--;
			}

			//removing tail
			if(index == size-1) {
				tail = tail.getPrev();
				tail.getNext().setPrev(null);
				tail.setNext(null);
				size--;
			}

			//remove regular Node
			Node currentNode = head.getNext();
			for (int i = 2; i <= size; i++) {
					if(i == index) {
						currentNode.previous.next = currentNode.next;
						currentNode.next.previous = currentNode.previous;
						currentNode.previous = null;
						currentNode.next = null;
						size--;
					}
			}
	} //remove

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.previous != null) {
			current = current.previous;
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} //DoublyLinkedList (class)
