//Zach Leonardo
//Lab 5

public class Prog1 {

  public static void main(String[] args) {

    /*
    int N = 1000;
    int N = 10000;
    int N = 50000;
    */int N = 100000;

    long startTime = System.nanoTime();
    String myString = new String();
    for(int i = 0; i < N; i++) {
      myString = myString + "a";
    }
    long endTime = System.nanoTime();
    long timeElapsed = endTime - startTime;

    System.out.println(timeElapsed + " nanoseconds");

    //String builder loop:

    startTime = System.nanoTime();
    StringBuilder mySB = new StringBuilder();
    for(int i = 0;i < N; i++) {
      mySB.append("a");
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;

    System.out.println(timeElapsed + " nanoseconds");

  }
}
