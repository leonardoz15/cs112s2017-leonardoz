//Zach Leonardo
//Lab 5
import java.util.Iterator;
import java.util.LinkedList;
public class Prog2 {

  public static void main(String[] args) {

    /*int N = 1000;
    int N = 10000;
    int N = 50000;
    */int N = 100000;
    
    //add to array
    long startTime = System.nanoTime();
    int[] myArray = new int[N];
    for (int i = 0; i < N; i++) {
        myArray[i] = i;
    }
    long endTime = System.nanoTime();
    long timeElapsed = endTime - startTime;
    System.out.println("Add to array: "+timeElapsed + " nanoseconds");

    // Add to Java’s LinkedList
    startTime = System.nanoTime();
    LinkedList<Integer> myList = new LinkedList<Integer>();
    for (int i = 0; i < N; i++) {
        myList.add(i);
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;
    System.out.println("Add to Java's LinkedList: "+timeElapsed + " nanoseconds");

    //Add to our LinkedList
    startTime = System.nanoTime();
    LL mySList = new LL();
    for (int i = 0; i < N; i++) {
        mySList.add(i);
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;
    System.out.println("Add to our LinkedList: "+timeElapsed + " nanoseconds");

    //Add to our IteratingGenericLL
    startTime = System.nanoTime();
    IteratingGenericLL<Integer> myIList = new IteratingGenericLL<Integer>();
    for (int i = 0; i < N; i++) {
        myIList.add(i);
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;
    System.out.println("Add to our IteratingGenericLL: "+timeElapsed + " nanoseconds");

    //Get from array
    startTime = System.nanoTime();
    for (int i = 0; i < N; i++) {
      int something = myArray[i];
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;
    System.out.println("Get from array: "+timeElapsed + " nanoseconds");

    //Get from java's LinkedList
    startTime = System.nanoTime();
    for (int i = 0; i < N; i++) {
      int variable = myList.get(i);
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;
    System.out.println("Get from Java's LinkedList: "+timeElapsed + " nanoseconds");

    //Get from our LinkedList
    startTime = System.nanoTime();
    for (int i = 0; i < N; i++) {
      int somethingElse = mySList.get(i);
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;
    System.out.println("Get from our LinkedList: "+timeElapsed + " nanoseconds");

    //Get from our IteratingGenericLL
    startTime = System.nanoTime();
    Iterator<Integer> iter = myIList.iterator();
    while (iter.hasNext()) {
        int anotherThing = iter.next();
    }
    endTime = System.nanoTime();
    timeElapsed = endTime - startTime;
    System.out.println("Get from our IteratingGenericLL: "+timeElapsed + " nanoseconds");

  }
}
