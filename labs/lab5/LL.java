public class LL {

	private class Node {
		private Node next;
		private int data;

		public Node() {
			next = null;
			data = 0;
		} //Node (constructor)

		public Node(int d, Node n) {
			next = n;
			data = d;
		} //Node (constructor)

		public int getData() {
			return data;
		} //getData

		public Node getNext() {
			return next;
		} //getNext

		public void setData(int newData) {
			data = newData;
		} //setData

		public void setNext(Node newNext) {
			next = newNext;
		} //setNext

	} //Node (class)


	// LL stuff starts here
	private int size;
	private Node head, tail;

	public LL() {
		head = null;
		tail = null;
		size = 0;
	} //LL (constructor)

	public void add(int d) {
		Node newNode = new Node(d, null); // create Node, add data

		if (size == 0) {
			head = newNode;
			tail = newNode; //update tail reference
			size++; //increment size
			return;
		} //if

		tail.next = newNode; // make tail point to new node
		tail = newNode; // update tail reference
		size++;

	} //add

	public void addHead(int d) {




	} //addHead

	public void add(int index, int d) {

	} //add

	public int get(int index) {
		Node currentPosition = head;
		int counter = 0;

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- get");
			return -1;
		} //if

		while (counter < index/* && index >= 0 && index < size*/) {
			currentPosition = currentPosition.next;
			counter++;
		} //while

		return currentPosition.data;
	} //get

	public void remove(int index) {
		// 4 cases:  removing head, removing tail, removing single node, regular

		// check to make sure the index is less than size
		if (index >= size || index < 0) {
			System.out.println("Hey you gave me bad input -- remove");
			return;
		} //if

		// 1 - removing head
		if (index == 0) {
			head = head.next;

			// 3 - removing single node
			if (size == 1) {
				tail = null;
			} //if
		} else {
			// 4 - removing a regular node
			Node currentLocation, previousLocation;
			currentLocation = head.next;
			previousLocation = head;
			int counter = 1;

			while (counter < index) {
				currentLocation = currentLocation.next;
				previousLocation = previousLocation.next;
				counter++;
			} //while

			previousLocation.next = currentLocation.next;
			//previousLocation.setNext(currentLocation.getNext());

			// 2 - removing tail
			if (index == size-1) {
				tail = previousLocation;
			} //if

		} //if-else

		size--;
	} //remove

	public int size() {
		return size;
	} //size

} //LL (class)
