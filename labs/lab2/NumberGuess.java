//Zachary Leonardo
//CMPSC 112
//Lab #2
//*********************************************************
import java.util.Random;
import java.util.Scanner;

public class NumberGuess {

    public static void main(String[] args) {
        Random rand = new Random();
        Scanner scan = new Scanner(System.in);

        while (true) {
        //Generate the random number:
        int randNum = rand.nextInt(100)+1;

        System.out.println("Hello Player! Try to guess my number between 1 and 100!");
        int counter = 0;


         while (true) {
             System.out.print("Your guess: ");
             int userGuess = scan.nextInt();
            if(userGuess > randNum){
                counter++;
                System.out.println("Your guess was to high!");
            }
            else if(userGuess < randNum){
                counter++;
                System.out.println("Your guess was to low!");
            }
            else{
                counter++;
                break;
            }

        }
        System.out.println("You got it in "+counter+" tries! My number was "+randNum+".");
        System.out.println("Would you like to play again (y/n)?");
        String again = scan.next();
        if(again.equals("y")){
            System.out.println("Generating another number...");
        }
        else{
            break;
        }

        }

    }
}
