//Zachary Leonardo
//CMPSC 112
//Lab #2
//**********************************************************************
import java.util.Scanner;
import java.util.Random;

public class CPUNumberGuess {

    public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      Random rand = new Random();

      while(true) {
        System.out.println("Hello Player! Please pick a number between 1 and 100.");
        int counter = 0;
        int guess = rand.nextInt(100)+1;
        int lower = 0;
        int upper = 100;
        int response;
        System.out.println("My first guess is "+guess+". How did I do?");

      while(true) {
        response = scan.nextInt();
        if(response == 1){
          counter ++;
          upper = guess-1;
          guess = rand.nextInt(upper-lower+1)+lower;
          System.out.println("My guess was to high. My next guess is "+guess+". How did I do?");
        }
        else if(response == -1){
          counter ++;
          lower = guess+1;
          guess = rand.nextInt(upper-lower+1)+lower;
          System.out.println("My guess was to low. My next guess is "+guess+". How did I do?");
        }
        else if(response == 0){
          counter++;
          break;
        }
        else {
          System.out.println("Enter a 1 for too high, 0 for correct, or -1 for too low");
        }
      }
      System.out.println("Yay, I got it in "+counter+" tries! Your number was "+guess+".");
      System.out.print("Would you like me to guess again? (y/n)? ");
      String again = scan.next();
      if(again.equals("y")){
        System.out.println("Restarting...");
      }
      else{
        break;
      }
    }
  }
}
