//Zach Leonardo
//CMPSC 112 - Lab #4
import java.util.Iterator;

public class SelectionSort {

  public static void main(String args[]) {

    int[] array = {8, 3, 4, 2, 1};
    int j,i;
    PrintArray(array);

  for(i = 0; i < array.length; i++){
    int minIndex = i;
    for(j = i+1; j < array.length; j++){

      if(array[j] < array[minIndex]){
        minIndex = j;
        }
      }
    int num = array[minIndex];
    array[minIndex] = array[i];
    array[i] = num;

    PrintArray(array);
    }
  }

  public static void PrintArray(int[] array) {

  for(int i = 0; i < array.length; i++){
    System.out.print(array[i]+" ");
    }
  System.out.println();
  }
}
