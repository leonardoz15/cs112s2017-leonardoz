//Zach Leonardo
//Lab 4
import java.util.Scanner;

public class CaesarCipher {

  public static final String Alphabet = "abcdefghijklmnopqrstuvwxyz";

  public static void main(String[] args) {

    Scanner scan = new Scanner(System.in);
    System.out.println("Enter a string for encription");
    String input = scan.nextLine();
    System.out.println("Enter the integer for shifting");
    int offset = scan.nextInt();

    System.out.println(encrypt(input, offset));
    System.out.println(decrypt(encrypt(input, offset), offset));
  }

  public static String encrypt(String message, int shiftDist) {
    message = message.toLowerCase();
    String encryptedText = ""; //blank string for adding char to
    for(int i = 0; i < message.length(); i++) {
      //if(message.charAt(i) == ' ') {
        //int p = i;
        //i++;
    //  }
      int position = Alphabet.indexOf(message.charAt(i));
      int newPos = (shiftDist + position) % 26;
      char replacement = Alphabet.charAt(newPos);
      encryptedText += replacement;
    } //for
    // encrytedText = (encryptedText.substring(0,int p) + " " + encryptedText.substring(p, encryptedText.length()));
    return encryptedText;
  }

  public static String decrypt(String encryptedText, int shiftDist) {
    encryptedText = encryptedText.toLowerCase();
    String decryptedText = ""; //blank string for decryption
    for(int i = 0; i < encryptedText.length(); i++) {
      int position = Alphabet.indexOf(encryptedText.charAt(i));
      int newPos = (position - shiftDist) % 26;
      if(newPos < 0) {
        newPos += 26;
      }
      char replacement = Alphabet.charAt(newPos);
      decryptedText += replacement;
    }
    return decryptedText;
  }
}
