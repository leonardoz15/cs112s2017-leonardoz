import java.util.Stack;

class Calculator {

  public static void main(String args[]) {
    String toCheck = "64-1-";
    calculator(toCheck);
  }

  public static int calculator(String postfix) {
    Stack<Integer> calculator = new Stack<Integer>();

    for (int i = 0; i < postfix.length(); i++) {
      char t = postfix.charAt(i);

      if (Character.isDigit(t)) { //adds digit to the stack
        int p = Character.getNumericValue(t);
        calculator.push(p);
      }
      else if (t == '+') {
        int a = calculator.pop();
        int b = calculator.pop();
        int g = a + b;
        calculator.push(g);
      }
      else if (t == '-') {
        int a = calculator.pop();
        int b = calculator.pop();
        int g = b - a;
        calculator.push(g);
      }
      else if (t == '*') {
        int a = calculator.pop();
        int b = calculator.pop();
        int g = a * b;
        calculator.push(g);
      }
      else if (t == '/') {
        int a = calculator.pop();
        int b = calculator.pop();
        int g = b / a;
        calculator.push(g);
      }
    }
    System.out.println("Your solution is: "+calculator.peek());
    return calculator.peek();
  }
}
