import java.util.Stack;

class Converter {

    public static void main(String[] args) {
      String toCheck = "5*3*4-7";
      boolean success = check(toCheck);
      if (success == true) {
        converter(toCheck);
      }
      else {
        System.out.println("Infix string is invalid");
      }
    }

    public static boolean check(String input) {
      Stack<Character> checker = new Stack<Character>();

      for (int i = 0; i < input.length(); i++) {
        char c = input.charAt(i);

        if(c == '(' || c == '[' || c == '{') {
          checker.push(c);
        } else if (c == ')') {
            if (checker.empty()) {
                return false;
            }
            char top = checker.pop();
            if (top != '(') {
                return false;
            }

        } else if (c == ']') {
            if (checker.empty()) {
              return false;
            }
            char top = checker.pop();
            if (top != '[') {
              return false;
            }
        } else if (c == '}') {
          if (checker.empty()) {
              return false;
          }
          char top = checker.pop();
          if (top != '{') {
              return false;
            }
        }
    }

    if (checker.empty()) {
      return true;
    } else {
      return false;
    }
}

//warning: its messy
    public static String converter(String infix) {
        Stack<Character> converter = new Stack<Character>();
        String postfix = "";

        for (int i = 0; i < infix.length(); i++) {
          char t = infix.charAt(i);

          if (Character.isDigit(t)) { //Adds digit
            postfix += t;
          }
          else if (t == '/' || t == '*') {
            if (converter.empty()) { //empty case
              converter.push(t);
            }
            else if (converter.peek() == '*' || converter.peek() == '/') { //same level case
            postfix += converter.pop();
            converter.push(t);
            }
            else {
              converter.push(t);
            }
          }
          else if (t == '+' || t == '-') {
            if (converter.empty()) { //empty case
              converter.push(t);
            }
            else if (converter.peek() == '+' || converter.peek() == '-') { //same level case
            postfix += converter.pop();
            converter.push(t);
            }
            else if (converter.peek() == '*' || converter.peek() == '/') {
              postfix += converter.pop();
              converter.push(t);
            }
          }
          else if (t == '(' || t == '{' || t == '[') { //for parenthesis
            converter.push(t);
          }
          else if (t == ')' || t == '}' || t == ']') {
            while(converter.peek() != '(') {
              postfix += converter.pop();
            }
          }

        }
        while (converter.empty() == false) {
          if(converter.peek() != '(') { //to account for the extra "("
            postfix += converter.pop();
          }
          else {
            break;
          }
        }
        System.out.println(postfix);
        return postfix;
    }
}
