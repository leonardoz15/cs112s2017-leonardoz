//Zach Leonardo
//Simon Burrows
public class Rect extends Quad {

  public Rect(double width, double height) {
    this.width=width;
    this.height=height;
  }

  public void calculateArea(){
    area = width*height;
  }

  public void calculatePerimeter(){
    perimeter = 2*(width + height);
  }
}
