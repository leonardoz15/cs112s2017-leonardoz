//Zach Leonardo
//Simon Burrows
public class Square extends Rect {

  public Square(double width) {
    super(width,0);
  }
  public void calculateArea(){
    area = width*width;
  }

  public void calculatePerimeter(){
    perimeter = width*4;
  }
}
