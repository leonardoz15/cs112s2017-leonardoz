//Zachary Leonardo
//Simon Burrows
import java.lang.Math;
public abstract class Shape {

    protected static double area;
    protected static double perimeter;

    public Shape() {
    area = 0;
    perimeter = 0;
    }

    public abstract void calculateArea();



    public abstract void calculatePerimeter();



    public double getArea() {
      return area;
    }

    public double getPerimeter() {
      return perimeter;
    }
}
