//Zach Leonardo
//Simon Burrows
public class Circle extends Round {

  protected double radius;

  public Circle(double radius) {
    this.radius = radius;
  }

  public void calculateArea() {
    area = pi * (radius*radius);
  }

  public void calculatePerimeter() {
    perimeter = 2*pi*radius;
  }
}
