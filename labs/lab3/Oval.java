//Zach Leonardo
//Simon Burrows
public class Oval extends Round {

  protected double major;
  protected double minor;

  public Oval(double major, double minor) {
    this.major = major;
    this.minor = minor;
    }

  public void calculateArea() {
    area = (pi)*(major)*(minor);
  }

  public void calculatePerimeter() {
    perimeter = (2)*(pi)*(Math.sqrt(((major*major)+(minor*minor))/2));
  }
}
