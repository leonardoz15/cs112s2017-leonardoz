//Zach Leonardo
//Simon Burrows
public class Trapzd extends Quad {

  private double otherBase;
  private double side1;
  private double side2;

  public Trapzd(double width, double height, double otherBase, double side1, double side2) {
    this.width=width;
    this.height=height;
    this.otherBase = otherBase;
    this.side1=side1;
    this.side2=side2;
  }

  public void calculateArea(){
    area = height *((width+otherBase)/2);
  }

  public void calculatePerimeter(){
    perimeter = width +  otherBase + side1 + side2;
  }
}
